export const gameArray = [
  {
    categories: [
      'top',
      'slots',
      'new'
    ],
    id: 'NETHEWISHMASTER',
    image: '//stage.whgstage.com/scontent/images/games/NETHEWISHMASTER.jpg',
    name: 'The Wish Master'
  }
];

export const jackpotArray = [
  {
    game: 'NETHEWISHMASTER',
    amount: 82337
  }
];
