import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Game} from '../../models/game';
import {GamesService} from '../../services/games.service';
import {NavigationStart, Router} from '@angular/router';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  providers: [GamesService]
})
export class MainPageComponent implements OnInit {

  games$: Observable<Game[]> = this.gamesService.getGames$();
  selectedCategory$: Observable<string> = this.router.events.pipe(
    filter(e => e instanceof NavigationStart),
    map(((event: NavigationStart) => event.url.slice(1)))
  );

  constructor(private gamesService: GamesService, public router: Router) {
  }

  ngOnInit(): void {
    this.gamesService.getAllGames(this.router.url.slice(1));
    this.selectedCategory$.subscribe(url => this.gamesService.changeCategory(url));
  }

}
