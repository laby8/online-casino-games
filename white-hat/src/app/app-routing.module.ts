import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app-component/app.component';
import {MainPageComponent} from './containers/main-page/main-page.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'top',
  pathMatch: 'full'
},
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: ':type',
        component: MainPageComponent
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
