import {__TEST__, assignJackpots} from './gameUtils';
import {RibbonType} from '../models/ribbon.enum';
import {gameArray, jackpotArray} from '../../../mocks/game.mocks';
import {Game} from '../models/game';

describe('GamesUtils', () => {
  // Redeclaring variables to set correct type
  const singleGameArray = gameArray as Game[];
  const singleJackpotArray = jackpotArray;

  describe('handleDefaultCategories', () => {
    it('should return empty array if no games are provided', () => {
      expect(__TEST__.handleDefaultCategories([], '')).toEqual([]);
    });
    it('should return empty array if no category is provided', () => {
      expect(__TEST__.handleDefaultCategories(singleGameArray, '')).toEqual([]);
    });
  });

  describe('handleJackpotCategory', () => {
    it('should return empty array if no jackpots are provided', () => {
      expect(__TEST__.handleJackpotCategory([], [])).toEqual([]);
    });
    it('should return empty array if game does not have a jackpot', () => {
      const noJackpotGame = [{...singleGameArray[0], id: 'no-jackpot'}];
      expect(__TEST__.handleJackpotCategory(noJackpotGame, singleJackpotArray)).toEqual([]);
    });
    it('should return one element array if game does have a jackpot', () => {
      expect(__TEST__.handleJackpotCategory(singleGameArray, singleJackpotArray).length).toEqual(1);
    });
  });

  describe('handleOtherCategory', () => {
    it('should return empty array if no games are provided', () => {
      expect(__TEST__.handleOtherCategory([])).toEqual([]);
    });
    it('should return one element array if contains one of other categories', () => {
      const gamesWithOtherCategories = [{...singleGameArray[0], categories: ['fun']}];
      expect(__TEST__.handleOtherCategory(gamesWithOtherCategories).length).toEqual(1);
    });
  });

  describe('shouldHaveRibbon', () => {
    it('should return true if game has category new', () => {
      expect(__TEST__.shouldHaveRibbon(singleGameArray[0], 'slots', RibbonType.NEW)).toBeTrue();
    });
    it('should return false if game has category top, and the category is top', () => {
      const gameWithTop = {...singleGameArray[0], categories: ['top']};
      expect(__TEST__.shouldHaveRibbon(gameWithTop, 'top', RibbonType.NEW)).toBeFalse();
    });
  });

  describe('getRibbonCategory', () => {
    it('should return undefined if game does not contain categories top or new', () => {
      const noRibbonCategoriesGame = {...singleGameArray[0], categories: ['fun']};
      expect(__TEST__.getRibbonCategory(noRibbonCategoriesGame, 'new')).toBeUndefined();
    });
  });

  describe('setRibbonCategory', () => {
    it('should return set ribbonCategory TOP for one game', () => {
      const gameList = [...singleGameArray];
      __TEST__.setRibbonCategories(gameList, 'new');
      expect(gameList[0].ribbonCategory).toEqual(RibbonType.TOP);
    });
    it('should not set any ribbonCategory', () => {
      const gameList = [{...singleGameArray[0], categories: ['slots']}];
      __TEST__.setRibbonCategories(gameList, 'new');
      expect(gameList[0].ribbonCategory).toEqual(undefined);
    });
  });

  describe('assignJackpots', () => {

    it('should assign jackpot value to game', () => {
      expect(assignJackpots(singleGameArray, singleJackpotArray)[0].jackpotAmount).toEqual(singleJackpotArray[0].amount);
    });

    it('should not assign jackpot value to game if singleJackpot has different game', () => {
      const jackpotNoCorrectGame = [{...singleJackpotArray[0], game: 'Wrong title'}];
      expect(assignJackpots(singleGameArray, jackpotNoCorrectGame)[0].jackpotAmount).toEqual(0);
    });
  });

});
