import {Game} from '../models/game';
import {Jackpot} from '../models/jackpot';
import {RibbonType} from '../models/ribbon.enum';

const otherCategories = ['fun', 'ball', 'virtual'];

export function filterGames(games: Game[], jackpots: Jackpot[], category: string): Game[] {

  let handler = filterHandlers[category];
  if (!handler) {
    handler = (gamesArg, _, categoryArg) => handleDefaultCategories(gamesArg, categoryArg);
  }
  const filteredGames = handler(...arguments);
  setRibbonCategories(filteredGames, category);
  return filteredGames;
}

const filterHandlers = {
  other: handleOtherCategory,
  jackpots: handleJackpotCategory
};

function handleOtherCategory(games: Game[]): Game[] {

  const gamesByCategories = new Map<string, Game[]>();
  games.forEach(game => {
    game.categories.forEach(category => {
      const previousGameValue = gamesByCategories.get(category) || [];
      gamesByCategories.set(category, previousGameValue.concat([game]));
    });
  });
  const filteredCategories = [];
  otherCategories.forEach(category => {
    if (gamesByCategories.has(category)) {
      filteredCategories.push(...gamesByCategories.get(category));
    }
  });
  return filteredCategories;
}

function handleJackpotCategory(games: Game[], jackpots: Jackpot[]): Game[] {
  if (!jackpots) {
    return [];
  }

  const gamesById = new Map(games.map(game => [game.id, game]));
  const gamesWithJackpots = [];

  for (const jackpot of jackpots) {
    if (gamesById.has(jackpot.game)) {
      gamesWithJackpots.push(gamesById.get(jackpot.game));
    }
  }
  return gamesWithJackpots;

}

function handleDefaultCategories(games: Game[], category: string): Game[] {
  return games.filter(game => game.categories.includes(category));
}

export function assignJackpots(games: Game[], jackpots: Jackpot[]): Game[] {
  const gamesById = new Map<string, Game>(games.map(game => {
    game.jackpotAmount = 0;
    return [game.id, game];
  }));

  for (const jackpot of jackpots) {
    if (gamesById.has(jackpot.game)) {
      gamesById.get(jackpot.game).jackpotAmount = jackpot.amount;
    }
  }
  return Array.from(gamesById.values());
}

function setRibbonCategories(games: Game[], category: string): void {
  games.forEach(game => game.ribbonCategory = getRibbonCategory(game, category));
}

function getRibbonCategory(game: Game, category: string): RibbonType | undefined {
  return Object.values(RibbonType).find(ribbonType => {
    if (shouldHaveRibbon(game, category, ribbonType)) {
      return ribbonType;
    }
  });
}

function shouldHaveRibbon(game: Game, category: string, ribbonName: RibbonType): boolean {
  const lowerCasedRibbonName = ribbonName.toLowerCase();
  return category !== lowerCasedRibbonName && game.categories.includes(lowerCasedRibbonName);
}

export const __TEST__ = {
  handleOtherCategory,
  handleJackpotCategory,
  handleDefaultCategories,
  shouldHaveRibbon,
  getRibbonCategory,
  setRibbonCategories,
  assignJackpots
};
