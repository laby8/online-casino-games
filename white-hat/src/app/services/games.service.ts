import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject, BehaviorSubject, timer} from 'rxjs';
import {Game} from '../models/game';
import {map, switchMap, takeUntil} from 'rxjs/operators';
import {assignJackpots, filterGames} from '../utils/gameUtils';
import {GamesApi} from '../api/games-api';
import {Jackpot} from '../models/jackpot';

@Injectable()
export class GamesService implements OnDestroy {

  constructor(private gamesApi: GamesApi) {
    this.refreshJackpots();
  }

  private readonly life$$ = new Subject<void>();
  private readonly games$$ = new BehaviorSubject<Game[]>([]);

  private jackpots: Jackpot[];
  private currentCategory: string;

  getGames$(): Observable<Game[]> {
    return this.games$$.pipe(map(games => filterGames(games, this.jackpots, this.currentCategory)));
  }

  getAllGames(category: string): void {
    this.currentCategory = category;
    this.gamesApi.getGames().subscribe(games => this.games$$.next(games));
  }

  changeCategory(category: string): void {
    this.currentCategory = category;
    this.games$$.next(this.games$$.value);
  }

  refreshJackpots(): void {
    const period = 3000;
    timer(0, period).pipe(
      takeUntil(this.life$$),
      switchMap(() => this.gamesApi.getJackpots())
    ).subscribe(jackpots => this.handleJackpots(jackpots));
  }

  handleJackpots(jackpots: Jackpot[]): void {
    this.jackpots = jackpots;
    let games = this.games$$.getValue();
    games = assignJackpots(games, this.jackpots);
    this.games$$.next(games);
  }

  ngOnDestroy(): void {
    this.life$$.next();
  }

}
