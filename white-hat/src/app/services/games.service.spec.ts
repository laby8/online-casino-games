import {discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {GamesService} from './games.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {skip, take} from 'rxjs/operators';
import {gameArray, jackpotArray} from '../../../mocks/game.mocks';
import {environment} from '../../environments/environment';

describe('GamesService', () => {
  const gamesUrl = `${environment.baseUrl}/games.php`;
  let service: GamesService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GamesService]
    });
    service = TestBed.inject(GamesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return empty array', () => {
    service.getGames$().pipe(take(1)).subscribe(array => expect(array.length).toEqual(0));
  });

  it('should return empty list, if no categories match',
    inject([HttpTestingController],
      (httpMock: HttpTestingController) => {

        service.getGames$().pipe(skip(1)).subscribe(data => {
          expect(data.length).toBe(0);
        });
        service.getAllGames('no-category');
        const gamesRequest = httpMock.expectOne(gamesUrl);
        expect(gamesRequest.request.method).toEqual('GET');
        gamesRequest.flush(gameArray);
      })
  );

  it('should return one element with jackpot',
    inject([HttpTestingController],
      fakeAsync((httpMock: HttpTestingController) => {

        service.getGames$().pipe(skip(2)).subscribe(data => {
          expect(data[0].jackpotAmount).toBe(82337);
        });
        service.refreshJackpots();
        tick(1000);
        service.getAllGames('new');
        const gamesRequest = httpMock.expectOne(gamesUrl);
        const jackpotsRequest = httpMock.expectOne(`${environment.baseUrl}/jackpots.php`);
        expect(gamesRequest.request.method).toEqual('GET');
        gamesRequest.flush(gameArray);
        jackpotsRequest.flush(jackpotArray);
        discardPeriodicTasks();
      })
    ));

  it('should setCurrentCategory in gamesService on urlChange',
    inject([HttpTestingController],
      (httpMock: HttpTestingController) => {

        service.changeCategory('new');
        // @ts-ignore
        expect(service.currentCategory).toEqual('new');
      })
  );

  it('should return no games filtered by non existing category',
    inject([HttpTestingController],
      (httpMock: HttpTestingController) => {

        service.getGames$().pipe(skip(1)).subscribe(data => {
          expect(data.length).toBe(0);
        });
        service.getAllGames('new');
        service.changeCategory('asd');
        const gamesRequest = httpMock.expectOne(gamesUrl);
        expect(gamesRequest.request.method).toEqual('GET');
        gamesRequest.flush(gameArray);
      })
  );

  it('should return list of one game',
    inject([HttpTestingController],
      (httpMock: HttpTestingController) => {

        service.getGames$().pipe(skip(1)).subscribe(data => {
          expect(data.length).toBe(1);
        });
        service.getAllGames('new');
        const gamesRequest = httpMock.expectOne(gamesUrl);
        expect(gamesRequest.request.method).toEqual('GET');
        gamesRequest.flush(gameArray);
      })
  );

  it('should return list of one game with ribbonCategory',
    inject([HttpTestingController],
      (httpMock: HttpTestingController) => {

        service.getGames$().pipe(skip(1)).subscribe(data => {
          expect(data[0].ribbonCategory).toBeTruthy();
        });
        service.getAllGames('new');
        const gamesRequest = httpMock.expectOne(gamesUrl);
        expect(gamesRequest.request.method).toEqual('GET');
        gamesRequest.flush(gameArray);
      })
  );
});
