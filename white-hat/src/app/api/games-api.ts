import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Game} from '../models/game';
import {HttpClient} from '@angular/common/http';
import {Jackpot} from '../models/jackpot';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GamesApi {

  constructor(private http: HttpClient) {
  }

  baseUrl = environment.baseUrl;

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(`${this.baseUrl}/games.php`);
  }

  getJackpots(): Observable<Jackpot[]> {
    return this.http.get<Jackpot[]>(`${this.baseUrl}/jackpots.php`);
  }

}
