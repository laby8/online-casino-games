import {Component, Input, OnInit} from '@angular/core';
import {RibbonType} from '../../models/ribbon.enum';

@Component({
  selector: 'app-ribbon',
  templateUrl: './ribbon.component.html',
  styleUrls: ['./ribbon.component.scss']
})
export class RibbonComponent implements OnInit {

  @Input() ribbonType: RibbonType;

  ngOnInit(): void {
  }

}
