import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-top-bar',
  templateUrl: './main-top-bar.component.html',
  styleUrls: ['./main-top-bar.component.scss']
})
export class MainTopBarComponent implements OnInit {

  categories = [
    {label: 'Top Games', route: 'top'},
    {label: 'New Games', route: 'new'},
    {label: 'Slots', route: 'slots'},
    {label: 'Jackpots', route: 'jackpots'},
    {label: 'Live', route: 'live'},
    {label: 'Blackjack', route: 'blackjack'},
    {label: 'Roulette', route: 'roulette'},
    {label: 'Table', route: 'classic'},
    {label: 'Poker', route: 'poker'},
    {label: 'Other', route: 'other'}
  ];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  toggleCategoriesList(categoriesMenu: HTMLElement): void {
    categoriesMenu.className = categoriesMenu.className ? '' : 'visible';
  }

  hideCategoriesList(categoriesMenu: HTMLElement): void {
    if (window.outerWidth < 1000) {
      categoriesMenu.className = '';
    }
  }

  get currentCategory(): string {
    return this.categories.find(category => this.router.url.endsWith(category.route))?.label;
  }

}
