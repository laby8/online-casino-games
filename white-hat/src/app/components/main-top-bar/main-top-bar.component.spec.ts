import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MainTopBarComponent} from './main-top-bar.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('MainTopBarComponent', () => {
  let component: MainTopBarComponent;
  let fixture: ComponentFixture<MainTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [MainTopBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set className to visible if its empty', () => {
    const categoriesMenu = {className: ''} as HTMLElement;
    component.toggleCategoriesList(categoriesMenu);
    expect(categoriesMenu.className).toEqual('visible');
  });

});
