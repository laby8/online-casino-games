import {RibbonType} from './ribbon.enum';

export interface Game {
  categories: string[];
  name: string;
  image: string;
  jackpotAmount: number;
  id: string;
  ribbonCategory: RibbonType;
}
