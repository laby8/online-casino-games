import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app-component/app.component';
import { MainPageComponent } from './containers/main-page/main-page.component';
import { GameCardComponent } from './components/game-card/game-card.component';
import { MainTopBarComponent } from './components/main-top-bar/main-top-bar.component';
import {HttpClientModule} from '@angular/common/http';
import { RibbonComponent } from './components/ribbon/ribbon.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    GameCardComponent,
    MainTopBarComponent,
    RibbonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
